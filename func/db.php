<?php
// $conn = oci_connect('spbo', 'spboadmin', 'localhost/xe');
// // $conn = oci_connect('spbo', 'spboadmin', '10.222.50.181/orcl');
// if (!$conn) {
//     $e = oci_error();
//     trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
// }
//
// $sql = 'SELECT * FROM SIT_TAG_STATUS';
// $stid = oci_parse($conn, $sql);
// // $didbv = 60;
// // oci_bind_by_name($stid, ':didbv', $didbv);
// oci_execute($stid);
// // $row = oci_fetch_array($stid, OCI_ASSOC);
// // print_r($row);
// while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
//     echo $row['TAG_SERIAL_NUM'] ."<br>";
// }
//
// // Output is
// //    Austin
// //    Ernst
// //    Hunold
// //    Lorentz
// //    Pataballa
//
// oci_free_statement($stid);
// oci_close($conn);

class Database
{
    public static $dbsid = 'xe' ;
   // public static $dbHost = '10.222.50.188/spbo' ;
        public static $dbHost = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.222.50.188)(PORT = 1521)) (CONNECT_DATA = (SERVICE_NAME = spbo) (SID = spbo)))' ;
   // public static $dbHost = '127.0.0.1/hqcs' ;
    public static $dbUsername = 'spbo';
    public static $dbUserPassword = 'spboadmin';

    public static $conn  = null;

      public function __construct() {
          die('Init function is not allowed');
      }

        public static function connect()
      {
         // One connection through whole application
         if ( null == self::$conn )
         {
          try
          {
            self::$conn = oci_connect(self::$dbUsername, self::$dbUserPassword, self::$dbHost);
          //  echo "connected!!";
            // self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword);
          }
          catch(PDOException $e)
          {
            die($e->getMessage());
          }
         }
         return self::$conn;
      }

      public static function disconnect()
      {
          self::$conn = null;
          oci_close($conn);
      }

      public static function getdb()
        {
          return  self::$dbHost;
        }

   public static function getusrnm()
      {
        return  self::$dbUsername;
      }

   public static function getpass()
      {
        return  self::$dbUserPassword;
      }

  }
?>
