<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="refresh" content="420">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SPBO Teras</title>
     <link rel="icon" type="image/png" href="../img/teras.png"/>
    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

      <!-- button print css -->
     <link href="../vendor/datatables/css/buttons.dataTables.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../dist/css/sidebar-toogle.css" rel="stylesheet">

<style type="text/css">
    canvas {
        background-color: white;
        width: auto;
        height: auto;
    }
</style>
</head>

<body>

  <div id="wrapper" class="active">
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                   <?php include '../func/sidetab_up2.php';?>

               </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> RFID Tag <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>

        </nav>
      <!-- Sidebar -->
            <!-- Sidebar -->
      <div id="sidebar-wrapper">
      <ul id="sidebar_menu" class="sidebar-nav">
        <?php include '../func/sidetab_up.php';?>

      </ul>
        <ul class="sidebar-nav" id="sidebar">
        <?php include '../func/sidetab.php';?>
        </ul>
      </div>

      <!-- Page content -->
      <div id="page-content-wrapper">
        <!-- Keep all page content within the page-content inset div! -->
        <div class="page-content inset">
          <div class="row">
              <div class="col-md-16">
                <div class="col-lg-12">
                     <p class="well lead">RFID TAG INFO</p>
                      <p>TNG Status</p>
                     <table width="100%" class="table table-striped table-bordered table-hover table-responsive" id="dataTables-ipstatus">
                         <thead>
                             <tr>
                                 <th>IP</th>
                                 <th>Status</th>

                             </tr>
                         </thead>
                          <tbody>
                         </tbody>
                     </table>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php
            include "../func/func.php";
            $tab_statinfo = "V_DASH_TAGSTATINFO";
            $sql_statinfo = 'SELECT QUEUETAG, ACKTAG, LATESTUPDATE FROM '.$tab_statinfo;

            $tab_txnmain = "V_DASH_TXNMAIN";
            $sql_txnmain = 'SELECT  TOTALTXN, TOTALTXNRSP, TOTALTXNRSPALL, TOTALTXNRSPGOOD, TOTALTXNRSPBAD FROM '.$tab_txnmain;

             $tab_suminfo = "V_DASH_TXNSUMINFO";
             $sql_suminfo = 'SELECT PLAZANO, QUEUETXN, ACKNOWLEDGETXN, RESPONSETXN FROM '.$tab_suminfo. ' ORDER BY PLAZANO ASC';

             $func = new func_db();
             $data_txnmain = $func::getdata($sql_txnmain);
             $data_statinfo = $func::getdata($sql_statinfo);
             $data_suminfo = $func::getdata($sql_suminfo);
             $db = $func::getcurr_db();
          // print_r($data);
            ?>

                <div class="col-lg-18">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                Table : <?php  echo $tab_txnmain; ?>
                        </div>
                        <div class="panel-body" >
                        <table  >
                        <tr   ><td width="40%" >
                         <canvas id="pie_tot_rsp"></canvas> </td><td  width="51%"  >

                           <canvas  id="pie_rsp_gd_bd" ></canvas> </td>
                            </tr></table>
                       </div>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover table-responsive" id="dataTables-tagtxnmain">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>TOTALTXN</th>
                                        <th>TOTALTXNRSP</th>

                                        <!-- <th>TOTALTXNRSPALL</th> -->
                                          <th>TOTALTXNRSPGOOD</th>
                                            <th>TOTALTXNRSPBAD</th>

                                    </tr>
                                </thead>
                                <tbody>
                                  <?php

                                  // $tot_txn = array();
                                  // $tot_rsp_gd_bd = array();


                                  $x=1;
                                foreach($data_txnmain as $dat)
                                  {
                                  echo ' <tr class="odd gradeX">';
                                  // echo '<td>'.$dat["RUNNING_NUMBER"].'</td>';
                                  echo '<td>'.$x.'</td>';
                                  echo '<td>'.$dat["TOTALTXN"].'</td>';
                                    echo '<td>'.$dat["TOTALTXNRSP"].'</td>';
                                              // echo '<td>'.$dat["TOTALTXNRSPALL"].'</td>';
                                                echo '<td>'.$dat["TOTALTXNRSPGOOD"].'</td>';
                                                  echo '<td>'.$dat["TOTALTXNRSPBAD"].'</td>';

                                  echo '</tr>';
                                    $val_TOTALTXN =  ( int ) $dat["TOTALTXN"];
                                    $val_TOTALTXNRSP =  ( int ) $dat["TOTALTXNRSP"];
                                    $lbl_TOTALTXNRSP =  $dat["TOTALTXNRSP"];
                                     $lbl_TOTALTXN_TOTALTXNRSP = array("TOTALTXN","TOTALTXNRSP");
                                  $lbl_TOTALTXN = $dat["TOTALTXN"];

                                  $tot_labeltxn = array("TOTALTXN","TOTALTXNRSP");
                                  $tot_rsp_gd_bd = array($dat["TOTALTXNRSPGOOD"],$dat["TOTALTXNRSPBAD"]);
                                    $tot_labelrsp_gd_bd = array("TOTALTXNRSPGOOD","TOTALTXNRSPBAD");

                                  $x+=1;
                                  }
                                  ?>

                                </tbody>
                            </table>
                            <!-- /.table-responsive -->

                        </div>
                        <div class="panel-heading">
                                Table : <?php  echo $tab_statinfo; ?>
                        </div>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover table-responsive" id="dataTables-tagstatinfo">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>QUEUETAG</th>
                                        <th>ACKTAG</th>

                                        <th>LATESTUPDATE</th>

                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $x=1;
                                foreach($data_statinfo as $dat)
                                  {
                                  echo ' <tr class="odd gradeX">';
                                  // echo '<td>'.$dat["RUNNING_NUMBER"].'</td>';
                                  echo '<td>'.$x.'</td>';
                                  echo '<td>'.$dat["QUEUETAG"].'</td>';
                                    echo '<td>'.$dat["ACKTAG"].'</td>';
                                              echo '<td>'.$dat["LATESTUPDATE"].'</td>';

                                  echo '</tr>';
                                  $x+=1;
                                  }
                                  ?>

                                </tbody>
                            </table>
                            <!-- /.table-responsive -->

                        </div>
                         <div class="panel-heading">
                                Table :  <?php  echo $tab_suminfo; ?>
                        </div>
                         <div class="panel-body" >
                         <table  width="100%"  >
                         <tr   ><td width="33%" >
                          <canvas id="pie_que"></canvas> </td><td width="33%" >
                           <canvas id="pie_ack"  ></canvas> </td><td width="33%"  >
                            <canvas  id="pie_res" ></canvas> </td>
                             </tr></table>

                            <canvas  id="charttest" >
                        </div>
                         <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover table-responsive" id="dataTables-tagsuminfo">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>PLAZANO</th>
                                        <th>QUEUETXN</th>

                                        <th>ACKNOWLEDGETXN</th>
                                             <th>RESPONSETXN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    $plz = array();
                                  $que = array();
                                  $ack = array();
                                  $res = array();
                                  $x=1;
                                foreach($data_suminfo as $dat)
                                  {
                                  echo ' <tr class="odd gradeX">';
                                  // echo '<td>'.$dat["RUNNING_NUMBER"].'</td>';
                                  echo '<td>'.$x.'</td>';
                                  echo '<td>'.$dat["PLAZANO"].'</td>';
                                    echo '<td>'.$dat["QUEUETXN"].'</td>';
                                              echo '<td>'.$dat["ACKNOWLEDGETXN"].'</td>';
                                                 echo '<td>'.$dat["RESPONSETXN"].'</td>';

                                  echo '</tr>';
                                  $plz[$x] = 'plz:'.$dat["PLAZANO"];
                                  $que[$x] = $dat["QUEUETXN"];
                                   $ack[$x] = $dat["ACKNOWLEDGETXN"];
                                   $res[$x] = $dat["RESPONSETXN"];

                                  $x+=1;
                                  }
                                  //print_r($plz);
                                  ?>

                                </tbody>
                            </table>
                            <!-- /.table-responsive -->

                        </div>


                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>


          </div>
        </div>


    </div>
    <!-- chartjs -->
     <script src="../js/Chart.bundle.js"></script>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- button print js -->
     <!-- <script src="../vendor/datatables/js/dataTables.buttons.min.js"></script> wan_disable -->
     <script src="../vendor/datatables/js/buttons.print.min.js"></script>

     <!-- button print pdf -->
     <script src="../vendor/datatables/js/pdfmake.min.js"></script>
     <script src="../vendor/datatables/js/vfs_fonts.js"></script>
     <!-- <script src="../vendor/datatables/js/buttons.flash.min.js"></script> -->
     <!-- <script src="../vendor/datatables/js/jszip.min.js"></script> -->

     <script src="../vendor/datatables/js/buttons.html5.min.js"></script>
     <!-- <script src="../vendor/datatables/js/buttons.print.min.js"></script> -->
      <!-- button print pdf -->

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>



    <script>


    $(document).ready(function() {
        $('#dataTables-tagstatinfo').DataTable({
           responsive: true,
              dom: 'Bfrtip',
           buttons: [
        {
            extend: 'print',
            message: 'ccccccccccccc',
            header: true,
            text: '* Print page *',
            title: 'SPBO --IJM--',
            messageTop:' <img src="../img/teras.png" width="60px">TERAS TEKNONOLOGI,SELANGOR <br> <b>Research and Development</b> ',
            messageBottom: 'RAID <br> Research and Development<br> <b>wan_dr@c</b>',
            autoPrint: true,
            exportOptions: {
                columns: ':visible',
            },
            customize: function (win) {
                $(win.document.body).find('table').addClass('display').css('font-size', '12px');
                $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                    $(this).css('background-color','#D0D0D0');
                });
                $(win.document.body).find('h1').css('text-align','center');
            }
        },
             {  text: 'CSV print',
                extend: 'csv',
                filename: 'tagstatinfo',
                orientation: 'landscape',
                pageSize: 'A4'
              }
      ]
        });

          $('#dataTables-tagsuminfo').DataTable({
             responsive: true,
              dom: 'Bfrtip',
           buttons: [
           {
            extend: 'print',
            message: 'ccccccccccccc',
            header: true,
            text: '* Print page *',
            title: 'SPBO --IJM--',
            messageTop:' <img src="../img/teras.png" width="60px">TERAS TEKNONOLOGI,SELANGOR <br> <b>Research and Development</b> ',
            messageBottom: 'RAID <br> Research and Development<br> <b>wan_dr@c</b>',
            autoPrint: true,
            exportOptions: {
                columns: ':visible',
            },
            customize: function (win) {
                $(win.document.body).find('table').addClass('display').css('font-size', '12px');
                $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                    $(this).css('background-color','#D0D0D0');
                });
                $(win.document.body).find('h1').css('text-align','center');
            }
        },
             {  text: 'CSV print',
                extend: 'csv',
                filename: 'tagsuminfo',
                orientation: 'landscape',
                pageSize: 'A4'
              }
        ]
        });

        //----------------
        $('#dataTables-tagtxnmain').DataTable({
           responsive: true,
            dom: 'Bfrtip',
         buttons: [
         {
          extend: 'print',
          message: 'ccccccccccccc',
          header: true,
          text: '* Print page *',
          title: 'SPBO --IJM--',
          messageTop:' <img src="../img/teras.png" width="60px">TERAS TEKNONOLOGI,SELANGOR <br> <b>Research and Development</b> ',
          messageBottom: 'RAID <br> Research and Development<br> <b>wan_dr@c</b>',
          autoPrint: true,
          exportOptions: {
              columns: ':visible',
          },
          customize: function (win) {
              $(win.document.body).find('table').addClass('display').css('font-size', '12px');
              $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                  $(this).css('background-color','#D0D0D0');
              });
              $(win.document.body).find('h1').css('text-align','center');
          }
      },
           {  text: 'CSV print',
              extend: 'csv',
              filename: 'txnmain',
              orientation: 'landscape',
              pageSize: 'A4'
            }
      ]
      });
        //-------------
    });



    </script>

    <script>
    var ctxP = document.getElementById("pie_que").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'doughnut',
        responsive: true,
        data: {
            labels: <?=json_encode(array_values($plz));?>,
            datasets: [
                {

                    data: <?=json_encode(array_values($que));?>,
                    backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
                }
            ]
        },
        options: {
            title: {
            display: true,
            text: 'QUEUETXN'
        },
            responsive: true
        }
    });
//----------------------------------
       var ctxP = document.getElementById("pie_ack").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'doughnut',
        responsive: true,
        data: {
            labels: <?=json_encode(array_values($plz));?>,
            datasets: [
                {

                    data: <?=json_encode(array_values($ack));?>,
                    backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
                }
            ]
        },
        options: {
            title: {
            display: true,
            text: 'ACKNOWLEDGETXN'
        },
            responsive: true
        }
    });
    //-----------------------------------------------

       var ctxP = document.getElementById("pie_res").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'doughnut',
        responsive: true,
        data: {
            labels: <?=json_encode(array_values($plz));?>,
            datasets: [
                {

                    data: <?=json_encode(array_values($res));?>,
                    backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
                }
            ]
        },
        options: {
            title: {
            display: true,
            text: 'RESPONSETXN'
        },
            responsive: true
        }
    });

    //----------------------------------bar chart
           var ctxP = document.getElementById("pie_tot_rsp").getContext('2d');
          var myPieChart = new Chart(ctxP, {
            type: 'bar',
            responsive: true,

           data: {
             labels: <?=json_encode(array_values($lbl_TOTALTXN_TOTALTXNRSP));?>,
             datasets: [{
                 label: 'TOTALTXN',
                 data: [<?=$val_TOTALTXN;?>],
                 backgroundColor: [
                     'rgba(255, 99, 132, 0.2)',

                 ],
                 borderColor: [
                     'rgba(255,99,132,1)',

                 ],
                 borderWidth: 1
             },
             {
                 label: 'TOTALTXNRSP',
                 data: [<?=$val_TOTALTXNRSP;?>],
                 backgroundColor: [
                     'rgba(75, 192, 192, 0.2)',

                 ],
                 borderColor: [
                     'rgba(75, 192, 192, 1)',

                 ],
                 borderWidth: 1
             }
           ]
         },

            options: {
              scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            },
                title: {
                display: true,
                text: 'Total TXN vs Total Response ,Total TXN: '+<?=$val_TOTALTXN;?>
            },
                responsive: true
            }
        });


        //-----------------------------------------------


            //----------------------------------
                var ctxP = document.getElementById("pie_rsp_gd_bd").getContext('2d');
                var myPieChart = new Chart(ctxP, {
                    type: 'pie',
                    responsive: true,
                    data: {
                        labels: <?=json_encode(array_values($tot_labelrsp_gd_bd));?>,
                        datasets: [
                            {

                                data: <?=json_encode(array_values($tot_rsp_gd_bd));?>,
                                backgroundColor: ["#EAF98C", "#90F9A1"],
                                hoverBackgroundColor: ["#EAF97C", "#90F9AA"]
                            }
                        ]
                    },
                    options: {
                        title: {
                        display: true,
                        text: 'Response Good vs Response Bad ,Total TXN: '+<?=$val_TOTALTXN;?>
                    },
                        responsive: true
                    }
                });
                //-----------------------------------------------


                //--------------test json pie--------------------
                $.getJSON( "data.php", function( data ) {

                  var Datax = [];
                  var labelsx = [];
                  $.each( data, function( key, val ) {
                      labelsx.push(key);
                      Datax.push(val);
                    });

                    var ctxP = document.getElementById("charttest").getContext('2d');
                    var myPieChart = new Chart(ctxP, {
                        type: 'pie',
                        responsive: true,
                        data: {
                            labels: labelsx,
                            datasets: [
                                {

                                    data: Datax,
                                    backgroundColor: ["#EAF98C", "#90F9A1"],
                                    hoverBackgroundColor: ["#EAF97C", "#90F9AA"]
                                }
                            ]
                        },
                        options: {
                            title: {
                            display: true,
                            text: 'test chart  '
                        },
                            responsive: true
                        }
                      });

                    });
                    //-----------------------------------------------



//---ajax interval
var dataTables =  $('#dataTables-ipstatus').DataTable( {
     ajax: '../func/getpingstatus.php',
     "searching": false,
      "paging":   false,
      "ordering": false,
       "info":     false

    } );
setInterval( function () {
   $('#dataTables-ipstatus').DataTable().ajax.reload();
    //  alert('hi');
 }, 30000 );
//--------
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
         });
</script>

</body>

</html>
